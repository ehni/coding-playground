////////////////////////////////////////////////////////////////
// For authoring Nightwatch tests, see
// https://nightwatchjs.org/guide
//
// For more information on working with page objects see:
//   https://nightwatchjs.org/guide/working-with-page-objects/
////////////////////////////////////////////////////////////////

module.exports = {
  beforeEach: browser => browser.init(),

  "e2e tests using page objects": browser => {
    const homepage = browser.page.homepage();
    homepage.waitForElementVisible("@appContainer");

    const app = homepage.section.app;
    app.assert.elementCount("@logo", 2);
    app.expect.section("@welcome").to.be.visible;
    app.expect
      .section("@headline")
      .text.to.match(/^Welcome to Your Vue\.js (.*)App$/);

    browser.end();
  },

  // not working...
  "fizzBuzz tests": browser => {
    const fizzBuzzPage = browser.page.fizzBuzzPage();
    fizzBuzzPage.waitForElementVisible("@gameContainer");

    const game = fizzBuzzPage.section.game;
    game.assert.section("@title").to.be.visible;
    game.assert.section("@explanation").to.be.visible;
    game.assert.section("@ruleset").to.be.visible;
    game.assert.section("@trySection").to.be.visible;

    browser.end();
  }
};

/**
 * A Nightwatch page object. The page object name is the filename.
 *
 * Example usage:
 *   browser.page.homepage.navigate()
 *
 * For more information on working with page objects see:
 *   https://nightwatchjs.org/guide/working-with-page-objects/
 *
 */

module.exports = {
  url: "/#/fizzbuzz",
  commands: [],

  elements: {
    gameContainer: "#fizzBuzzGameContainer"
  },

  sections: {
    game: {
      selector: "#fizzBuzzGameContainer",

      sections: {
        title: {
          selector: "#gameHeader"
        },

        explanation: {
          selector: "#gameRules"
        },

        ruleset: {
          selector: "#ruleSet",

          elements: {
            addItemButton: {
              selector: "#addRuleItem"
            }
          }
        },

        trySection: {
          selector: "#tryOutSection",

          elements: {
            numberInput: {
              selector: "#numberInput"
            },
            calculateButton: {
              selector: "#calculateButton"
            },
            result: {
              selector: "#result"
            }
          }
        }
      }
    }
  }
};
